<?php
require_once 'Product.php';
class Product{
    private $id_product;
    private $name;
    private $price;

    public function __construct($id_product, $name, $price){
        $this->id_product       = $id_product;
        $this->name             = $name;
        $this->price            = $price;
    }

  public function getIdprod()
  {
    return $this->id_product;
  }

  public function getName()
  {
    return $this->name;
  }
  
  public function getPrice()
  {
    return $this->price;
  }

}

?>