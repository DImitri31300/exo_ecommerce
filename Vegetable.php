<?php
require_once 'Product.php';
class Vegetable extends Product{
    private $productorName;
    private $expireAt;

    public function __construct($id_product, $name, $price,$productorName,$expireAt){
      parent::__construct($id_product, $name, $price);
      $this->productorName = $productorName;
      $this->expireAt = $expireAt;
    }

  public function getProd()
  {
    return $this->productorName;
  }

  public function getExpir()
  {
    return $this->expireAt;
  }

  public function isFrech(){

  }

  public function fresh(){
    $date = new DateTime();
    $date->format('Y-m-d');
    $date_actuel = date('Y-m-d');

    if($date_actuel === $date){
      echo "Votre produit est périmé";
    }
  }

  
  
}

?>