<?php
require 'Product.php';
require 'Vegetable.php';
require 'Cloth.php';

       
$date = new DateTime();
$date->setDate(2019,11,3);
$date->format('Y-m-d');


$date_actuel = date('Y-m-d');


$survet    = new Cloth(1,"Survetement",2.43,"Nike");
$tennis    = new Cloth(2,"Tennis",34.56,"Reebok");
$pull      = new Cloth(3,"Pull-Over",45.65,"Ralph Lauren");
$pants     = new Cloth(6,"Pantalon",45.87,"Levis"); 

$aubergine = new Vegetable(4,"Aubergine",2.56, "Les jardins d'Alice","2019-12-03");
$citron    = new Vegetable(5,"Citron", 3.98, "La  Agrumiers","2019-11-23");
$carotte   = new Vegetable(7,"carotte",0.78,"Les jardins d'Alices",2019-11-03);

$tab_basket= [
    $survet,
    $tennis,
    $pull,
    $aubergine,
    $citron,
    $pants,
    $carotte
];

?>