<?php
require_once 'Product.php';
class Cloth extends Product{
    private $brand;

    public function __construct($id_product, $name, $price, $brand){
      parent::__construct($id_product, $name, $price);
        $this->brand = $brand;
    }

  public function getBrand()
  {
    return $this->brand;
  }

  public function try(){
      //code
  }
}

?>