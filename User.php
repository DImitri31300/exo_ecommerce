<?php

class User{
    private $id_user;
    private $email;
    private $createAt;

    public function __construct($i, $j, $k){
        $this->id_user       = $i;
        $this->email    = $j;
        $this->createAt = $k;
    }

  public function getId()
  {
    return $this->id_user;
  }

  public function getMail()
  {
    return $this->email;
  }
  
  public function getCreateAt()
  {
    return $this->createAt;
  }

}

?>